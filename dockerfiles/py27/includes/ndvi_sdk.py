import matplotlib.pyplot as plt
from datetime import date
import ipyleaflet as ipyl
import ipywidgets as ipyw
#import eodag.api.core.SatImagesAPI as satimagesapi
#import eodag as dag2
from eodag.api.core import EODataAccessGateway

base_dir = '/Tuto/sources'

dag = EODataAccessGateway(user_conf_file_path='%s/eodagconf.yml' % base_dir)
product_type = 'S2_MSI_L1C'
#Rome area 12.355175322237528 41.99011864571753,12.610607451143778 41.99011864571753,12.610607451143778 41.796399810893845,12.355175322237528 41.796399810893845,12.355175322237528 41.99011864571753
extent = {
  'lonmin': 12.155175,
  'lonmax': 12.810607, 
  'latmin': 41.596399,
  'latmax': 42.190118
}
# Toulouse
extent2 = {
  'lonmin': 1.306000,
  'lonmax': 1.551819,
  'latmin': 43.527642,
  'latmax': 43.662905
}
lonmin = extent['lonmin'] + 0.001
lonmax = extent['lonmax'] - 0.001
latmin = extent['latmin'] + 0.001
latmax = extent['latmax'] - 0.001
print(lonmin, latmin, lonmax, latmax)
dag.set_preferred_provider(provider='airbus-ds')
print("Searching for matching products")
products = dag.search(product_type,

startTimeFromAscendingNode='2018-05-01',
completionTimeFromAscendingNode=date.today().isoformat(),
geometry=extent,
cloudCover=1)

nbprods = len(products)
if (nbprods >= 1) :
  print("Found " + str(nbprods) + " matching products, using first one")
  product = products[0]
else:
  print("No products found, aborting")

print("Starting NDVI computation")
#myResolution = 0.0001
myResolution = 0.00001

VIR = product.get_data(crs='epsg:4326', resolution=myResolution, band='B04', extent=(lonmin, latmin, lonmax, latmax))
NIR = product.get_data(crs='epsg:4326', resolution=myResolution, band='B08', extent=(lonmin, latmin, lonmax, latmax))
NDVI = (NIR - VIR * 1.) / (NIR + VIR)

plt.imshow(NDVI, cmap='RdYlGn', aspect='auto')
plt.savefig('%s/ndvi.png' % base_dir)
