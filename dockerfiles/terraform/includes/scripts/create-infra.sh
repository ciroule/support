source ${PWD}/setenv.sh
${PWD}/gen_params.sh

source ${PWD}/creds

cd ${terraconf}
# Generate infra creation plan
terraform plan --var-file=${terraconf}/parameters.tfvars --out ${terraconf}/myplan.out

# Run the plan
echo "Apply plan (y/n) ?"
read rep
if [ "$rep" == "y" ] ; then
 terraform apply "${terraconf}/myplan.out"
fi

