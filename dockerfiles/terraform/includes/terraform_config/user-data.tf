data "template_file" "mount_datavol" {
  template = "${file("mount_datavol.sh")}"
}

data "template_cloudinit_config" "config" {
  gzip          = true
  base64_encode = true

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.mount_datavol.rendered}"
  }
}
