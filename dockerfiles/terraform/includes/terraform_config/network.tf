/*resource "flexibleengine_vpc_v1" "vpc" {
  name = "${var.vpc_name}"
  cidr = "${var.vpc_cidr}"
}

resource "flexibleengine_vpc_subnet_v1" "subnet" {
  name = "${var.vpc_subnet}"
  cidr = "${var.vpc_subnet_cidr}"
  gateway_ip = "${var.vpc_subnet_gateway_ip}"
  primary_dns = "${var.vpc_subnet_primary_dns}"
  secondary_dns = "${var.vpc_subnet_secondary_dns}"
  vpc_id = "${flexibleengine_vpc_v1.vpc.id}"
}
*/

resource "flexibleengine_networking_network_v2" "network" {
  name           = "${var.network_name}"
  admin_state_up = "true"
}

resource "flexibleengine_networking_subnet_v2" "subnet" {
  name       = "${var.subnet_name}"
  network_id = "${flexibleengine_networking_network_v2.network.id}"
  cidr       = "${var.subnet_cidr}"
  ip_version = 4
  dns_nameservers = "${var.dns_list}"
}

resource "flexibleengine_networking_router_v2" "vpc" {
  name             = "${var.vpc_name}"
  external_gateway = "0a2228f2-7f8a-45f1-8e09-9039e1d09975"
}

resource "flexibleengine_networking_router_interface_v2" "k8s_router_interface" {
  router_id = "${flexibleengine_networking_router_v2.vpc.id}"
  subnet_id = "${flexibleengine_networking_subnet_v2.subnet.id}"
}
