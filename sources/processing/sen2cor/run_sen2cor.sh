#!/bin/bash

export dkimg="registry.gitlab.com/pub-sobloo/support/sen2cor:1.0.0"
export mntpath="/home/cloud/tmp" #mounted directory of the HOST

docker stop sen2cor
docker rm sen2cor

# run docker script
docker run -v ${mntpath}:/work -it --name sen2cor \
  -e BASE_PATH="/work" \
  ${dkimg} /app/l1c_processing.py
