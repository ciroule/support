import logging
from logging.handlers import RotatingFileHandler
from logging import handlers
import json_log_formatter
import sys, os
#from logstash_formatter import LogstashFormatter

class PyLogger():

        def __init__(self, loglevel,loggerName,outputDir=None):
            self.loglevel = loglevel
            self.myLogger = logging.getLogger(loggerName)
            if outputDir is None:
                outputDir = "/var/log"

            self.initLoggers2(outputDir + "/" + loggerName + ".log")

        def getLogger(self):
          return(self.myLogger)

        def info(self,msg):
            self.log("INFO",msg)

        def debug(self,msg):
            self.log("DEBUG",msg)

        def error(self,msg):
            self.log("ERROR",msg)

        def log(self,level=None, msg=None, extra=None):
         if extra is None:
           extra = {'typelog':'regular'}
         if level is None:
           self.myLogger.debug(msg, extra)
         elif level == 'INFO':
           self.myLogger.info(msg, extra)
         else:
           self.myLogger.error(msg, extra)

        def initLoggers2(self, logfile):

          logLevel = logging.DEBUG
          self.myLogger.setLevel(logLevel)
          # create file handler which logs even debug messages
          fh = logging.FileHandler(logfile)
          fh.setLevel(logLevel)
          # create console handler with a higher log level
          ch = logging.StreamHandler(sys.stdout)
          ch.setLevel(logLevel)
          # create formatter and add it to the handlers
          #formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
          formatter = json_log_formatter.JSONFormatter()
          #formatter = LogstashFormatter()
          ch.setFormatter(formatter)
          fh.setFormatter(formatter)
          # add the handlers to logger
          self.myLogger.addHandler(ch)
          self.myLogger.addHandler(fh)

        def initLoggerFile(self, logfile):

         LOGFILE = logfile

         global_level = logging.DEBUG
         handler = logging.StreamHandler()

         handler.setLevel(global_level)
         formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
         #formatter = logging.Formatter('%(message)s')
         handler.setFormatter(formatter)
         self.myLogger.addHandler(handler)

         fh = handlers.RotatingFileHandler(LOGFILE, maxBytes=(1048576*5), backupCount=7)
         fh.setFormatter(formatter)
         self.myLogger.addHandler(fh)
