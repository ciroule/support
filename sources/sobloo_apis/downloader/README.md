```
#######################################################################
# Airbus Defence and Space (SpS)
# Project: Dias
# sw name: massdwnld
#######################################################################
# Version	Date		Author
# 0.1		2019/06/05	B. Figueres
#######################################################################
```

Introduction
--  
This sw demonstrate the search API & download capabilites within sobloo.  
The search allows finding products by:
 - geonames
 - polygon (WKT)
 - satellite kind
 - product kind
 - sensing start date
 - sensing end date

The search is done with __gintersect__ option that means that any object intersecting the polygon
specified will be selected.  
The polygon must be specified without _spaces_ to be correctly passed to the container. For instance a
point defined by `-8.278040784499353 10.493595909047388` should be passed as `-8.278040784499353+10.493595909047388`.  

Once the products found, the script downloads the number of product specified on a
mounted directory of the host.  
Look at the script __rundownloads.sh__ that features a full example to get
S2 L1C products over the Ivory coast (WKT) for products acquired between 
20 and 21 march 2019

How to
--
To run the software, simply edit the script rundownloads.sh after having updated the parameters:
 - POLYGON
 - SATELLITE
 - TYPE_PROD
 - SENSING_START
 - SENSING_END
 - NB_DWNLD
 - OUTPUT_PATH
 - SB_APIKEY

In case of issues feel free to contact sobloo support at https://sobloo.atlassian.net/servicedesk/customer/user/login?destination=portals
  
Enjoy !





