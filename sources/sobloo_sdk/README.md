Jupyter Notebook Samples
==
There following samples are Jupyter Notebooks that use the SDK Eodag. 

- Basics : Search and download of a product from the catalogue 
- Bandmath : Search, download and NDVI calculation 


Further documentation for the SDK Eodag can be found here : https://eodag.readthedocs.io/en/latest/
